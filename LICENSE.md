Copyright 2018 Jhonatan Vieira - Diego Ramos

Este software ha sido desarrollado para su uso dentro de la Dirección General de Recursos Hídricos, dependiente de la Secretaría de Ambiente, Desarrollo Sostenible y Cambio Climático de Tierra del Fuego, Argentina.

El uso de este software, ya sea para su copia parcial o total, modificación, fusión, publicación, distrubución o comercialización solo estará bajo la autorización y acuerdo de los desarrolladores Jhonatan Vieira (jhonatanvieira@live.com.ar) y Diego Ramos (diego.pablodiegor@gmail.com).

