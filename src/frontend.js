//var inicio = require('./principal');
import {inicio} from './principal.js'
import {testGprs,obtenerHost,abrirPuerto,cerrarPuerto} from './src/gprs.js'
import {muestraCal,getDatosTReal} from './src/calibracion.js'
import {getDatosMemoria,abrirPuertoD,cerrarPuertoD} from './src/descargaDatos.js'
//import createMenu from './main-window/menu'

window.addEventListener('load', () => {
  console.log('funciona');
  addViewsEvents()
})

function addViewsEvents(){
  const views = document.querySelectorAll('a.nav-group-item')
  //console.log(views)
  for (let i = 0, length1 = views.length; i < length1; i++) {
      views[i].addEventListener('click', function () {
        //console.log(views[i])
        changeView(this)
      })
    }
}

function changeView (node) {
    // selecciono el elemento con active y lo borro de su atributo clase
    document.querySelector('a.active').classList.remove('active')
    // al nodo le agrego el active dentro de su atributo class
    node.classList.add('active')
    //la lógica lo puedo sacar desde el icon de atributo class del tag span
    console.log(node.querySelector('span').classList[1])

    switchViews(node.querySelector('span').classList[1])
    
    // por último cambio el el contenido de display container
    //document.getElementById('display-container').innerHTML = '<h2>Datos en tiempo real</h2> <div id="data"></div>'
    
}



function switchViews(iconView){

  switch(iconView) {
    case 'icon-home':
        document.getElementById('display-container').innerHTML = '<div id="error"></div><div id="ports"></div> <div class="row"> <div class="col-md-6"> <div class="input-group"><label>Puerto</label><select id="puertoSelect" class="form-control"></select></div></div></div><div class="row"><div class="col-md-6"><div class="button-group"><button class="btn btn-primary btn-lg" id="butt1">Conectar puerto</button><button class="btn btn-primary btn-lg" id="cerrar">cerrar puerto</button></div></div></div><h2>Estado Conexion</h2><div id="conexion"></div>'
        inicio();
        break;
    case 'icon-download':
        document.getElementById('display-container').innerHTML = '<h2>Descarga de datos</h2>  <button class="btn btn-primary btn-lg" id="butt2">Descargar datos</button> <table class="table-striped"> <thead> <tr> <th>Fecha</th> <th>Hora</th> <th>Altura</th> </tr> </thead> <tbody id="tabla-cuerpo"> </tbody> </table>'
        document.querySelector('#butt2').addEventListener('click', getDatosMemoria);
        

        break;
    case 'icon-signal':
        document.getElementById('display-container').innerHTML = '<h2>GPRS</h2> <div id="dirhost"></div> <button class="btn btn-primary btn-lg" id="testGprs">Iniciar Test</button-group>  <div id="gprs-id"><div class="form-group">  <label>Monitor GSM</label> <textarea class="form-control" rows="9"></textarea></div></div>'
        document.querySelector('#testGprs').addEventListener('click', testGprs);
        obtenerHost();
        break;
    case 'icon-tools':
        document.getElementById('display-container').innerHTML = '<h2>Calibración</h2> <div id="resultadoCal"><div class="form-group"><label>Pendiente</label><input id="pendiente" value="" type="email" class="form-control" placeholder=""></div></div><div id="resultadoCal1"><div class="form-group"><label>Ordenade de origen</label><input id="pendiente" value="" type="email" class="form-control" placeholder=""></div></div><h2>Datos en tiempo real</h2><button class="btn btn-primary btn-lg" id="butt">Datos en tiempo real</button> <div id="data"></div><canvas id="myChart"></canvas> '
        muestraCal()
        //muestraChart()
        document.querySelector('#butt').addEventListener('click', getDatosTReal);
        break;
    case 'icon-hourglass':
        document.getElementById('display-container').innerHTML = '<h2>Timers</h2> <button class="btn btn-primary btn-lg" id="timers">Iniciar Test</button> <div id="data"></div>'

        document.querySelector('#timers').addEventListener('click', timers);
        break; 
    case 'icon-drive':
        document.getElementById('display-container').innerHTML = '<h2>Reiniciar memoria</h2> <div id="data"></div>'
        break;  
    case 'icon-clock':
        document.getElementById('display-container').innerHTML = '<h2>Ajuste de reloj</h2> <div id="data"></div>'
        break;     
  }
}





function timers(){

  port.write('timers', function(err, results) {
           if (err){
            console.log("error")
           }else{
            console.log("no error")
           }
           //console.log('Error: ' + err);
           port.on('data',function(data, results){
            console.log(data.toString())
            console.log(results)
          });
           console.log('Results ' + results);
      });
 
    
    

    
  port.pipe(parser);

  parser.on('data', function(data){     

    document.getElementById('data').innerHTML = data.toString();


  });
  
}